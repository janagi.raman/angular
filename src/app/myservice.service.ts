import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MyserviceService {
  std:Student[]=[
    {name:"Billa",age:24,email:"billa@email.com"},
    {name:"Phil",age:23,email:"phil@gmail.com"},
    {name:"Alan",age:24,email:"alan@gmail.com"}
  ]

  constructor() { 

  }
  sendData(){
    console.log("Sending data...");
    return this.std;
  }
}
class Student{
  name:String;
  age:Number;
  email:String;
}
