import { Component } from '@angular/core';
import {MyserviceService}from './myservice.service'


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private myser:MyserviceService) { 
    // Initialization inside the constructor

 }
 res:any;
 receiveData(){
   this.res=this.myser.sendData();
   console.log(this.res);
 }

  title:String ="Angular";
  data:String="{{topic}}";
  pro:String='[innerHTML]="topic1"';
  style:String="[style.color]='red' ";
  topic:String="Data Binding";
  fName:String="Janagi";
  lName:String="Raman";
  topic1:String="Property Binding"; 
  topic2:String="Style Binding"; //use[style.color] =" 'red' " format like css single time of element 
  topic3:String="Class Binding";  //Group of Element  use of class name to access css more than one time
  topic4:String="Attribute Binding"; //
  x:Number=2;
//------------------------------------------
  topic5:String="Two Way binding";      //<input type="text"[(ngModel)]="y"> <br> <span>{{y}}</span>
  y:String="";                          //[(ngModel)]-->Current Value
  z=this.y;   //optional  because code overload  it's refernce for just uderstand the cow flow                
  
  //Event Binding         
  msg:String="Hiii....";            //<span>{{mag}}</span>
  myEvent(){                        //<button (click)="myEvent()">click me!</button>
   this.msg="Bye.....";
  }
  


  //Get user input,$event
  msge:String="";                  //<input type="text" (keyup)="my_Event($event)">
  my_Event(x:any){                 //(keyup)-->to get current type value
    console.log(x);                //($event)-->is object eg:(x:any)
    this.msge=x.target.value;      //x.target.value--> to get the user input value to assign the variable
  }
  //Template Refernce Variable #
  messg:String="";                 //<span>{{messg}}</span><br>
  my_Value(y:any){                 //<input type="text" #test (keyup)="my_Value(test.value)">
    console.log(y);                
    this.messg=y.target;      
  }    
  //Directives
  /*
  *Component Directives-->Mainly used HTML templete,(html,css,ts) are final app component @component decorator,templete for DOM layout
  *Structural Directives-->behaviour,changes, (*)prefixmanipulation of DOM elements
  *Attribute Directives
  *Custom Directives
  */  
 
  /*Structural Directives
  -->ngIf
  -->ngElse
  */
 //ngIf Directives                                    <p *ngIf="b; else tel">{{msg1}}</p>
 msg1:String="Janagi Raman";             // <button (click)="btn()">Click Me!</button>
 error:String="Error this page"; //else  // <ng-template #tel>{{error}}</ng-template>
 b:Boolean=false;                        // *ngIf; else variable name
 btn(){                                  // ng-templeate means(else) to call else value in this page
  this.b=true;
 }
//NgStyle-->using to set multiple inline styles for the HTML (group of property)
//NgClass-->using the class attribute of the HTML element to apply the style (each class have group of property)

//ngStyle  
name:String="Janagi Raman";
  font:String="bold";
  size:Number=10;
  myStyle(){
    let obj={                         //<p [ngStyle]="myStyle()">{{name}}</p>
      "font-weight":this.font,
      "font-size":this.size
    }
    return obj;
  }
 //ngClass
 myClass(){
  let obj={                         //<p [ngClass]="myClass()">{{name}}</p>
    myfont:true,                    //using the css file
    mysize:true,
    mycolor:true
    }
  return obj;
  }

  //Custom Directive

 //Promise & Observable
 //Promise --> A single Value  it is aysnc ,could not cancel a request to the API,not retry a failed call,hard to mange
 //Observables -->Observable More than one value it is sync & aysnc, it is better
 //Pending

 //Pipes
 //takes the data as input & Transferms desired output
 //Custom Pipe
 //create custom pipe eg: mypipe,pipe.ts
 //import{Pipe,PipeTransform} from '@angular/core';
 /*
 @Pipe({
  name:'srt'
  })
 */
/*
export class MyPipe implements PipeTransform
{
  transform(x:any)
  {
    return x*x;
  }
}
*/
//<p>{{12 | sqr}}</p>


 




 //ngFor Directives
 bks:Books[]=[
   {id:1001,title:"Devil",author:"Hitler"},
   {id:1002,title:"History",author:"Bose"},
   {id:1003,title:"War",author:"Bhutin"}
 ]

}

//ngFor Directives
export class Books{
   id:number;
   title:string;
   author:string;
}


