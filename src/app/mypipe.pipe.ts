import{Pipe,PipeTransform} from '@angular/core';

@Pipe({
  name:'sqr'
  })

export class MyPipe implements PipeTransform
{
  transform(y:any)  //transform is default key
  {
    return y*y;
  }
}