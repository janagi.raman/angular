import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {MyserviceService}from './myservice.service';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { MydirectiveDirective } from './mydirective.directive';
import{MyPipe}from './mypipe.pipe';
@NgModule({
  declarations: [
    AppComponent,
    MydirectiveDirective,
    MyPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
  
  ],
  providers: [MyserviceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
